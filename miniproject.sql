CREATE SCHEMA `miniproject` DEFAULT CHARACTER SET utf8mb4 ;
CREATE TABLE actors(
	`id` BIGINT UNSIGNED,
    `username` VARCHAR(50),
    `password` VARCHAR(50),
    `role_id` INT UNSIGNED,
    `is_verified` ENUM('true','false'),
    `is_active` ENUM('true','false'),
	`created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `modified_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT `actorsPK` PRIMARY KEY (`id`),
    CONSTRAINT `role_idFK` FOREIGN KEY (`role_id`) REFERENCES actor_roles(`id`)
);

CREATE TABLE customers(
	`id` BIGINT UNSIGNED,
    `first_name` VARCHAR(50),
    `last_name` VARCHAR(50),
    `email` VARCHAR(50),
    `avatar` VARCHAR(200),
	`created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `modified_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT `customersPK` PRIMARY KEY (`id`)
);

CREATE TABLE actor_roles(
	`id` INT UNSIGNED,
    `role_name` VARCHAR(50),
    CONSTRAINT `actor_rolesPK` PRIMARY KEY (`id`)
);

CREATE TABLE register_approvals(
	`id` INT UNSIGNED,
    `admin_id` BIGINT UNSIGNED,
    `super_admin_id` BIGINT UNSIGNED,
	`status` VARCHAR(50),
    CONSTRAINT `register_approvalsPK` PRIMARY KEY (`id`)
);

INSERT INTO actor_roles(`id`,`role_name`) VALUES(1, 'super_admin'),(2, 'admin'), (3, 'customer');
INSERT INTO actors(`id`, `username`, `password`, `role_id`, `is_verified`, `is_active`) VALUES (1, "super_admin", "super_admin", 1, true, true);
CREATE USER 'superadmin'@0.0.0.0 IDENTIFIED BY "superpassword";
GRANT ALL PRIVILEGES ON miniproject.* TO "super_admin"@0.0.0.0;

CREATE USER 'superadminlocalhost'@'localhost' IDENTIFIED BY 'superpassword';
GRANT ALL PRIVILEGES ON miniproject.* TO 'superadminlocalhost'@'localhost';
